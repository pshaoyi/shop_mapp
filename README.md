# 小程序商城项目
基于微信小程序的商城前端

# 功能
该项目以[ShirneCMS](https://gitee.com/shirnecn/ShirneCMS)为接口端实现原生小程序的商城系统<br />

 - [x] 首页,产品页，新闻页
 - [x] 授权登录
 - [x] 产品详情
 - [x] 购物车
 - [x] 会员中心
 - [x] 收货地址
 - [ ] 下单支付
 - [ ] 订单管理

# 后端项目
[ShirneCMS](https://gitee.com/shirnecn/ShirneCMS)

# 功能截图
![首页](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-index.png "首页")
![产品展示](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-list.png "产品展示")
![产品详情](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-detail.png "产品详情")
![产品选项](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-option.png "产品选项")
![购物车](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-cart.png "购物车")
![购物车编辑](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-cart-2.png "购物车编辑")
![会员中心](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-member.png "会员中心")
![地址管理](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-address-list.png "地址管理")
![地址编辑](https://shirne.oss-cn-shenzhen.aliyuncs.com/website-mapp/shop-address.png "地址编辑")